"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.anObjectLike = void 0;
const Matcher_1 = require("ts-mockito/lib/matcher/type/Matcher");
class AnObjectLike extends Matcher_1.Matcher {
    constructor(anObject) {
        super();
        this.anObject = anObject;
    }
    match(value) {
        const valueJson = JSON.stringify(value);
        const objectJson = JSON.stringify(this.anObject);
        return JSON.stringify(value) === JSON.stringify(this.anObject);
    }
    toString() {
        return "anObjectLike()";
    }
}
function anObjectLike(anObject) {
    return new AnObjectLike(anObject);
}
exports.anObjectLike = anObjectLike;
