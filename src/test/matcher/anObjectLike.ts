import { Matcher } from "ts-mockito/lib/matcher/type/Matcher";

class AnObjectLike extends Matcher {
  constructor(private anObject: any) {
    super();
  }

  public match(value: any): boolean {
    const valueJson = JSON.stringify(value);
    const objectJson = JSON.stringify(this.anObject);
    return JSON.stringify(value) === JSON.stringify(this.anObject);
  }

  public toString(): string {
    return "anObjectLike()";
  }
}

export function anObjectLike(anObject: any) {
  return new AnObjectLike(anObject) as any;
}
